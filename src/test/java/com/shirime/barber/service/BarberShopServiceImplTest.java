package com.shirime.barber.service;

import com.shirime.barber.BarberShopTestFactory;
import com.shirime.barber.config.ShirimePersistenceConfig;
import com.shirime.barber.dao.BarberShopDAO;
import com.shirime.barber.dao.ServiceDAO;
import com.shirime.barber.dao.TimePeriodDAO;
import com.shirime.barber.dto.*;
import com.shirime.barber.pojo.BarberShop;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ShirimePersistenceConfig.class)
@Transactional
public class BarberShopServiceImplTest {


    @PersistenceContext
    private EntityManager em;
    @Autowired
    private BarberShopService barberShopService;
    @Autowired
    private BarberShopDAO barberShopDAO;
    @Autowired
    private ServiceDAO serviceDAO;
    @Autowired
    private TimePeriodDAO timePeriodDAO;
    private UserDTO barber;

    @BeforeEach
    public void setUp() throws Exception {
        this.barber = this.createAndPersistBarberAndBarberShop();
    }

    @Test
    public void barberShopSave$alongWithOwner() throws Exception {
        List<BarberShop> barberShops = this.barberShopDAO.searchForProperties(this.barber.getBarberShop());
        assertEquals(0, barberShops.size());
    }

    @Test
    public void barberShopExistenceChecker$byName() throws Exception {
        assertTrue(this.barberShopService.checkIfNameExists("barbershop"));
        assertFalse(this.barberShopService.checkIfNameExists("barberstore"));
    }

    @Test
    public void barberShopExistenceChecker$byCnpj() throws Exception {
        assertTrue(this.barberShopService.checkIfCnpjExists("123456789/1011"));
        assertFalse(this.barberShopService.checkIfCnpjExists("0000000000/0000"));
    }

    @Test
    public void barberShopService$crud() {
        Long barberShopId = this.barber.getBarberShop().getId();
        assertEquals(0, this.barberShopService.getServices(barberShopId).size());
        this.barberShopService.saveService(barberShopId, new ServiceDTO("fade haircut", 15.00, 20));
        List<ServiceDTO> services = this.barberShopService.getServices(barberShopId);
        assertEquals(1, services.size());
        ServiceDTO service = services.get(0);
        service.setPrice(20.00);
        this.barberShopService.saveService(barberShopId, service);
        assertEquals(Double.valueOf(20.00), this.serviceDAO.getById(service.getId()).getPrice());
        this.barberShopService.removeService(service.getId());
        assertEquals(0, this.barberShopService.getServices(barberShopId).size());
    }

    @Test
    public void barberShopWorkHours$crud() {
        Long barberShopId = this.barber.getBarberShop().getId();
        String startTime = "2020-05-01T00:00:00";
        String endTime = "2020-05-01T23:59:00";
        assertEquals(0, this.barberShopService.getWorkHours(barberShopId, startTime, endTime).size());
        this.barberShopService.saveWorkHours(barberShopId, new TimePeriodDTO("2020-05-01T06:00:00", "2020-05-01T09:30:00"));
        List<TimePeriodDTO> allWorkHours = this.barberShopService.getWorkHours(barberShopId, startTime, endTime);
        assertEquals(1, allWorkHours.size());
        TimePeriodDTO workHours = allWorkHours.get(0);
        workHours.setEnd("2020-05-01T09:17:00");
        this.barberShopService.saveWorkHours(barberShopId, workHours);
        assertEquals("2020-05-01T09:17:00", this.timePeriodDAO.getById(workHours.getId()).getEnd().toString());
        this.barberShopService.removeWorkHours(workHours.getId());
        assertEquals(0, this.barberShopService.getWorkHours(barberShopId, startTime, endTime).size());
    }

    private UserDTO createAndPersistBarberAndBarberShop() throws Exception {
        AddressDTO addressDTO = BarberShopTestFactory.createAddressDTO();
        BarberShopDTO barberShopDTO = BarberShopTestFactory.createBarberShopDTO();
        barberShopDTO.setAddress(addressDTO);
        UserDTO barberDTO = BarberShopTestFactory.createBarberDTO();
        barberDTO.setBarberShop(barberShopDTO);
        barberDTO.getBarberShop().setId(this.barberShopService.saveBarberShop(barberDTO));
        return barberDTO;
    }

}