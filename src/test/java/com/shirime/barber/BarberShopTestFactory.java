package com.shirime.barber;

import com.shirime.barber.dto.AddressDTO;
import com.shirime.barber.dto.BarberShopDTO;
import com.shirime.barber.dto.UserDTO;
import com.shirime.barber.pojo.UserType;
import java.util.Date;

public class BarberShopTestFactory {

    public static AddressDTO createAddressDTO() {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setZipCode("12345-678");
        addressDTO.setState("state");
        addressDTO.setCity("city");
        addressDTO.setStreet("street");
        addressDTO.setNumber(123);
        addressDTO.setLatitude(Double.valueOf("12345678"));
        addressDTO.setLongitude(Double.valueOf("87654321"));
        return addressDTO;
    }

    public static BarberShopDTO createBarberShopDTO() {
        BarberShopDTO barberShopDTO = new BarberShopDTO();
        barberShopDTO.setName("barbershop");
        barberShopDTO.setCnpj("123456789/1011");
        return barberShopDTO;
    }

    public static UserDTO createBarberDTO() {
        UserDTO barberDTO = new UserDTO();
        barberDTO.setName("barber");
        barberDTO.setSurname("smith");
        barberDTO.setDateOfBirth(new Date());
        barberDTO.setCpf("123456789-10");
        barberDTO.setEmail("barber@barbershop.com");
        barberDTO.setLogin("login");
        barberDTO.setPassword("password");
        barberDTO.setType(UserType.BARBER.getCode());
        return barberDTO;
    }
 }
