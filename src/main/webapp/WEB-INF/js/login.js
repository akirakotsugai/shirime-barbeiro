var LoginHandler = function() {

    var openModalLogin = function() {
        $("#modal-login-title").html("Opa");
        $("#modal-login form").trigger("reset");
        ShirimeUtils.removeValidationsDiv("personal-info");
        ShirimeUtils.removeValidationsDiv("barberShop-info");
        ShirimeUtils.removeValidationsDiv("user-signin");
        $("#personal-info").hide();
        $("#barberShop-info").hide();
        $("#choose-user-type").hide();
        $("#user-signin").show();
        $("#modal-login").show();
        $("#btn-login").html("Entrar");
        $("#btn-login").show();
        $("#user-type-registration").val("");
    };

    var completeLocationFields = function() {
        var zipCode = $("#cep-registration").val();
        if (zipCode.length == 9) {
            $.ajax({
                url: "http://apps.widenet.com.br/busca-cep/api/cep/" + zipCode + ".json",
                type: "GET",
                success: completeFields
            });
        }
        else if (zipCode.length == 0) {
            ShirimeUtils.removeFieldValidation("cep-registration");
            ShirimeUtils.removeValidationsByClass("address-auto-completable");
        }
        else {
            ShirimeUtils.invalidateField("cep-registration");
        }
    };

    var completeFields = function(data) {
        if (data.status == 1) {
            $("#state-registration").val(data.state);
            $("#city-registration").val(data.city);
            $("#district-registration").val(data.district);
            $("#address-registration").val(data.address);
            ShirimeUtils.validateFieldsByClass("address-auto-completable");
            ShirimeUtils.validateField("cep-registration");
        }
        else {
            $("#state-registration").val("");
            $("#city-registration").val("");
            $("#district-registration").val("");
            $("#address-registration").val("");
            ShirimeUtils.invalidateField("cep-registration");
            ShirimeUtils.removeValidationsByClass("address-auto-completable");
        }
    };

    var checkPasswordSecurity = function() {
        if ($("#password-registration").val().length == 0) {
            ShirimeUtils.removeFieldValidation("password-registration");
        }
        else if ($("#password-registration").val().length < 6) {
            ShirimeUtils.invalidateField("password-registration");
        }
        else {
            ShirimeUtils.validateField("password-registration");
        }
    };

    var checkPasswordEquality = function() {
        if ($("#password-registration").val().length == 0 && $("#password-again-registration").val().length == 0) {
            ShirimeUtils.removeFieldValidation("password-again-registration");
        }
        else if ($("#password-registration").val() == $("#password-again-registration").val()) {
            ShirimeUtils.validateField("password-again-registration");
        }
        else {
            ShirimeUtils.invalidateField("password-again-registration");
        }
    };

    var validateCNPJ = function() {

    }

    return {
        completeLocationFields : completeLocationFields,
        checkPasswordEquality : checkPasswordEquality,
        checkPasswordSecurity : checkPasswordSecurity,
        openModalLogin : openModalLogin
    };

}();

var modal = document.getElementById("modal-login");
var btn = document.getElementById("btn-login");
var span = document.getElementsByClassName("modal-close")[0];
btn.onclick = function() {
        modal.style.display = "block";
}

span.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

document.getElementById("signup-link").addEventListener("click", function() {
    $("#user-signin").hide();
    $("#btn-login").hide();
    $("#choose-user-type").show();
    $("#modal-login-title").html("Quem é você?");
});

$("#i-am-a-barber").on("click", function() {
    $("#choose-user-type").hide();
    $("#personal-info").show();
    $("#btn-login").html("Próximo");
    $("#btn-login").show();
    $("#modal-login-title").html("Dados Pessoais");
    $("#user-type-registration").val(UserHandler.userType.barber);
});

$("#i-am-a-client").on("click", function() {
    $("#choose-user-type").hide();
    $("#personal-info").show();
    $("#btn-login").html("Cadastrar");
    $("#btn-login").show();
    $("#modal-login-title").html("Dados Pessoais");
    $("#user-type-registration").val(UserHandler.userType.client);
});

$("#btn-login").on("click", function() {
    if ($("#btn-login").html() == 'Entrar') {
        var login = $("#login-login");
        var password = $("#login-password");
        UserHandler.logIn(login, password);
        return;
    }
    var divDadosBarberShop = $("#barberShop-info");
    var divDadosPessoais = $("#personal-info");
    if ($("#btn-login").html() == 'Próximo') {
        ShirimeUtils.invalidateFieldsSemValidationDiv('personal-info');
        if (divDadosPessoais.find('.invalid').length == 0) {
            $("#btn-login").html("Cadastrar");
            $("#modal-login-title").html("Dados da BarberShop");
            divDadosPessoais.hide();
            divDadosBarberShop.show();
        }
    }
    else {
        ShirimeUtils.invalidateFieldsSemValidationDiv('personal-info');
        if (divDadosBarberShop.is(":visible")) {
            ShirimeUtils.invalidateFieldsSemValidationDiv('barberShop-info');
        }
        if (divDadosBarberShop.find('.invalid').length == 0) {
            var userType = $("#user-type-registration").val();
            UserHandler.registerUser(userType);
        }
    }
});

$("#cep-registration").mask("99999-999");
$("#cpf-registration").mask("999.999.999-99");
$("#cnpj-registration").mask("99.999.999/9999-99");
$("#date-of-birth-registration").mask("99/99/9999");

$("#cep-registration").on("keyup", function() {
    LoginHandler.completeLocationFields();
});

$("#login-registration").on("keyup", function() {
    var login = $("#login-registration").val();
    if (login.length == 0) {
        ShirimeUtils.removeFieldValidation("login-registration");
    }
    else if (login.length < 5) {
        ShirimeUtils.invalidateField("login-registration");
    }
    else {
        UserHandler.checkLoginAvailability(login);
    }
});

$("#password-registration").on("keyup", function() {
    LoginHandler.checkPasswordSecurity();
    LoginHandler.checkPasswordEquality();
});

$("#password-again-registration").on("keyup", function() {
    LoginHandler.checkPasswordEquality();
});

$("#cpf-registration").on("keyup", function() {
    var cpf = $("#cpf-registration").val();
    if (cpf.length == 0) {
        ShirimeUtils.removeFieldValidation("cpf-registration");
    }
    else if (cpf.length == 14) {
        UserHandler.checkCpfAvailability(cpf);
    }
    else {
        ShirimeUtils.invalidateField("cpf-registration");
    }
});

$("#name-registration").on("keyup", function() {
    if ($("#name-registration").val().length != 0) {
        ShirimeUtils.validateField("name-registration");
    }
    else {
        ShirimeUtils.removeFieldValidation("name-registration");
    }
});

$("#surname-registration").on("keyup", function() {
    if ($("#surname-registration").val().length != 0) {
        ShirimeUtils.validateField("surname-registration");
    }
    else {
        ShirimeUtils.removeFieldValidation("surname-registration");
    }
});

$("#email-registration").on("keyup", function() {
    if ($("#email-registration").val().length == 0) {
        ShirimeUtils.removeFieldValidation("email-registration");
    }
    else if (ShirimeUtils.isEmail($("#email-registration").val())) {
        ShirimeUtils.validateField("email-registration");
    }
    else {
        ShirimeUtils.invalidateField("email-registration");
    }
});

$("#number-registration").on("keyup", function() {
    if ($("#number-registration").val().length != 0) {
        ShirimeUtils.validateField("number-registration");
    }
    else {
        ShirimeUtils.removeFieldValidation("number-registration");
    }
});

$("#date-of-birth-registration").on("keyup", function() {
    if ($("#date-of-birth-registration").val().length == 0) {
        ShirimeUtils.removeFieldValidation("date-of-birth-registration");
    }
    else if ($("#date-of-birth-registration").val().length == 10) {
        ShirimeUtils.validateField("date-of-birth-registration");
    }
    else {
        ShirimeUtils.invalidateField("date-of-birth-igualregistration");
    }
});

$("#cnpj-registration").on("keyup", function() {
    var cnpj = $("#cnpj-registration").val();
    if (cnpj.length == 0) {
        ShirimeUtils.removeFieldValidation("cnpj-registration");
    }
    else if (cnpj.length == 18) {
        BarberShopHandler.checkCnpjAvailability(cnpj);
    }
    else {
        ShirimeUtils.invalidateField("cnpj-registration");
    }
});

$('#barbershop-name-registration').on("keyup", function() {
    var name = $('#barbershop-name-registration').val();
    if (name.length == 0) {
        ShirimeUtils.removeFieldValidation("barbershop-name-registration");
    }
    else if (name.length < 5) {
        ShirimeUtils.invalidateField("barbershop-name-registration");
    }
    else {
        BarberShopHandler.checkNameAvailability(name);
    }
});