var UserHandler = function() {

    var userType = Object.freeze({
        admin : 1,
        barber : 2,
        client : 3
    });

    var logIn = function(user, password) {
        params = ShirimeUtils.getParameters("user-signin");
        $.ajax({
            url: ShirimeUtils.appBaseUrl() + "/user/authenticate",
            type: "POST",
            data: params,
            success: function(response) {
                var mainPage = document.open("text/html", "replace");
                mainPage.write(response);
                mainPage.close();
            },
            error: function() {
                $("#authentication-error").show();
            }
        });
    };

    var registerUser = function(userType) {
        var userParams = ShirimeUtils.getParameters("personal-info");
        userParams['type'] = userType;
        if (userType == UserHandler.userType.barber) {
            var barberShopParams = ShirimeUtils.getParameters("barberShop-info");
            registerBarber(userParams, barberShopParams);
        }
        else if (userType == UserHandler.userType.client){
            registerClient(userParams);
        }
    };

    var registerBarber = function(userParams, barberShopParams) {
        var params = Object.assign({}, barberShopParams, userParams);
        $.post(
            ShirimeUtils.appBaseUrl() + "/user/register",
             params,
             function(response) {
                 $("#modal-login").hide();
                 alert( "Barber cadastrado com sucesso." );
             }
        ).fail(function() {
             $("#modal-login").hide();
             alert( "Desculpe, tente novamente." );
         });
    };

    var registerClient = function(userParams) {
        $.post(
            ShirimeUtils.appBaseUrl() + '/user/register',
             userParams,
             function(response) {
                 $("#modal-login").hide();
                 alert( "Usuário cadastrado com sucesso." );
             }
        ).fail(function() {
            $("#modal-login").hide();
            alert( "Desculpe, tente novamente." );
        });
    };

    var processUserSavedResponse = function(xhr) {
        if (xhr.status == 200) {
            alert("SALVOU");
        }
        else if(xhr.status == 400){
            alert("NÃO SALVOU");
        }
    };

    var checkLoginAvailability = function(login) {
        $.ajax({
            url: ShirimeUtils.appBaseUrl() + "/user/availability/login",
            type: "GET",
            data: {'login' : login},
            success: function() {
                ShirimeUtils.validateField("login-registration");
            },
            error: function() {
                ShirimeUtils.invalidateField("login-registration");
            }
        });
    };

    var checkCpfAvailability = function(cpf) {
        $.ajax({
            url: ShirimeUtils.appBaseUrl() + "/user/availability/cpf",
            type: "GET",
            data: {'cpf' : cpf},
            success: function() {
                ShirimeUtils.validateField("cpf-registration");
            },
            error: function() {
                ShirimeUtils.invalidateField("cpf-registration");
            }
        });
    };

    return {
        userType : userType,
        registerUser : registerUser,
        checkCpfAvailability : checkCpfAvailability,
        checkLoginAvailability : checkLoginAvailability,
        logIn : logIn
    };

}();
