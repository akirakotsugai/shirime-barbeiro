var ShirimeUtils = function() {

    var validateField = function(fieldId) {
        $("#"+fieldId).removeClass("invalid");
        $("#"+fieldId).addClass("valid");
    };

    var validateFieldsDiv = function(divId) {
        $("#" + divId + " [name]").each( function(index, element) {
            $(element).removeClass("invalid");
            $(element).addClass("valid");
        });
    }

    var invalidateField = function(fieldId) {
        $("#"+fieldId).removeClass("valid");
        $("#"+fieldId).addClass("invalid");
    };

    var removeFieldValidation = function(fieldId) {
        $("#"+fieldId).removeClass("valid");
        $("#"+fieldId).removeClass("invalid");
    }

    var fieldStatus = function(fieldId) {
        if ($("#"+fieldId).hasClass("valid")) {
            return 1;
        }
        else if ($("#"+fieldId).hasClass("invalid")) {
            return -1;
        }
        else {
            return 0;
        }
    };

    var getParameters = function(div) {
        //para conseguir pegar os valores precisa habilitar os fields desabilitados
        var desabilitados = $("#" + div).find(":input:disabled").removeAttr('disabled');
        data = $("#" + div + " [name]").serializeArray().reduce(function(obj, item) {
            if (item.value != "") {
            if (item.name == 'price') {
                item.value = item.value.replace('.','');
                item.value = item.value.replace(',','.');
            }
            if (item.name == 'minutes') {
                var timeLength = item.value.split(':');
                var hours = parseInt(timeLength[0]);
                var minutes = parseInt(timeLength[1]);
                item.value = minutes + hours * 60;
            }
                obj[item.name] = item.value;
            }
            return obj;
        }, {});
        desabilitados.attr('disabled','disabled');
        return data;
    };

    var removeValidationsDiv = function(div) {
        $("#" + div + " [name]").each( function(index, element) {
            $(element).removeClass("invalid");
            $(element).removeClass("valid");
        });
    };

    var invalidateFieldsSemValidationDiv = function(div) {
        $("#" + div + " [name]").each( function(index, element) {
            if (element.type != 'hidden' && !$(element).hasClass("invalid") && !$(element).hasClass("valid")) {
                $(element).addClass("invalid");
            }
        });
    }

    var isEmail = function(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    };

    var removeValidationsByClass = function(classe) {
        $("." + classe).each( function(index, element) {
            $(element).removeClass("invalid");
            $(element).removeClass("valid");
        });
    };

    var validateFieldsByClass = function(classe) {
        $("." + classe).each( function(index, element) {
            $(element).addClass("valid");
        });
    };

    var appBaseUrl = function() {
        return location.origin + '/ShirimeBarber';
    };

    var applyResponse = function(response, xhr, divId) {
        if (xhr.getResponseHeader('AUTH') == 'false') {
            top.location.href=ShirimeUtils.appBaseUrl();
        } else {
            $('#' + divId).html(response);
        }
    };

    return {
        applyResponse : applyResponse,
        appBaseUrl : appBaseUrl,
        validateField : validateField,
        invalidateField : invalidateField,
        removeFieldValidation : removeFieldValidation,
        fieldStatus : fieldStatus,
        getParameters : getParameters,
        removeValidationsDiv : removeValidationsDiv,
        isEmail : isEmail,
        invalidateFieldsSemValidationDiv : invalidateFieldsSemValidationDiv,
        removeValidationsByClass : removeValidationsByClass,
        validateFieldsByClass : validateFieldsByClass,
        validateFieldsDiv : validateFieldsDiv
    };

}();
