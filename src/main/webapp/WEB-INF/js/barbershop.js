var BarberShopHandler = function() {

    var C = {
        ihBarberShopId : 'barberShopId',
        divServiceRegistrationId : "service-registration",
        divTableServicesId : "services-table",
        btnCancelServiceRegistration : "btn-cancel-service-registration",
        btnSaveServiceRegistration : "btn-save-service-registration",
        inputServiceRegistrationDescriptionId : 'service-registration-description',
        inputServiceRegistrationPriceId : 'service-registration-price',
        inputServiceRegistrationTimeLengthId : 'service-registration-time-length'
    };

    var saveService = function() {
        params = ShirimeUtils.getParameters(C.divServiceRegistrationId);
        params['barberShop.id'] = $('#' + C.ihBarberShopId)[0].value;
        $.post(
            ShirimeUtils.appBaseUrl() + "/barbershop/services/save",
            params,
            function(data, status, xhr) {
               ShirimeUtils.applyResponse(data, xhr, C.divTableServicesId);
            }
        ).always(function() {
                hideServiceRegistration();
        });
    };

    var removeService = function(serviceId) {
        var params = {
            'barberShopId' : barberShopId = $('#' + BarberShopHandler.C.ihBarberShopId)[0].value,
            'serviceId' : serviceId
        }
        $.post(
            ShirimeUtils.appBaseUrl() + "/barbershop/services/remove",
            params,
            function(data, status, xhr) {
               ShirimeUtils.applyResponse(data, xhr, C.divTableServicesId);
            })
    }

    var checkCnpjAvailability = function(cnpj) {
        $.ajax({
            url: ShirimeUtils.appBaseUrl() + "/barbershop/availability/cnpj",
            type: "GET",
            data: {'cnpj' : cnpj},
            success: function() {
                ShirimeUtils.validateField("cnpj-registration");
            },
            error: function() {
                ShirimeUtils.invalidateField("cnpj-registration");
            }
        });
    };

    var checkNameAvailability = function(name) {
        $.ajax({
            url: ShirimeUtils.appBaseUrl() + "/barbershop/availability/name",
            type: "GET",
            data: {'name' : name},
            success: function() {
                ShirimeUtils.validateField("barbershop-name-registration");
            },
            error: function() {
                ShirimeUtils.invalidateField("barbershop-name-registration");
            }
        });
    };

    var hideServiceRegistration = function () {
        showHideServiceRegistration("hide");
    };

    var fillInServiceRegistrationForm = function (serviceId) {
        var serviceInfo = $('#service_' + serviceId);
        $('#' + C.inputServiceRegistrationDescriptionId).val(serviceInfo.find('.serviceDescription')[0].value);
        var price = serviceInfo.find('.servicePrice')[0].value.replace('.',',');
        if (price.split(',')[1].length == 1) price += '0';
        $('#' + C.inputServiceRegistrationPriceId).val(price);
        var serviceMinutes = serviceInfo.find('.serviceMinutes')[0].value;
        var hours = '' + parseInt(serviceMinutes / 60);
        var minutes = '' + serviceMinutes - hours * 60;
        if (hours.length == 1) hours = '0' + hours;
        if (minutes.length == 1) minutes = '0' + minutes;
        $('#' + C.inputServiceRegistrationTimeLengthId).val(hours + ':' + minutes);
    }

    var showServiceRegistration = function (serviceId) {
        var registrationTitle = $('#service-registration-title');
        var registrationId = $('#service-registration-id');
        ShirimeUtils.removeValidationsDiv('service-registration');
        if (serviceId) {
            registrationTitle.html('Edição');
            registrationId.val(serviceId);
            ShirimeUtils.validateFieldsDiv(C.divServiceRegistrationId);
            fillInServiceRegistrationForm(serviceId);
        }
        else {
            registrationTitle.html('Novo');
            registrationId.val('');
            $("#service-registration form").trigger("reset");
        }
        showHideServiceRegistration("show");
    };

    var showHideServiceRegistration = function(acao) {
        var btnAddServiceStyle = document.getElementById('btn-add-service').style
        var divServiceRegistrationStyle = document.getElementById("service-registration").style;
        var divTableServicesStyle = document.getElementById("services-table").style;
        btnAddServiceStyle.display = acao == 'show' ? "none" : "inline-block";
        divServiceRegistrationStyle.display = acao == 'show' ? "inline-block" : "none";
        divServiceRegistrationStyle.float = acao == 'show' ? "left" : "none";
        divTableServicesStyle.margin = acao == 'show' ? 0 : "auto";
        divTableServicesStyle.float = acao == 'show' ? "right" : "none";
    };

    return {
        C : C,
        saveService : saveService,
        removeService : removeService,
        checkCnpjAvailability : checkCnpjAvailability,
        checkNameAvailability : checkNameAvailability,
        hideServiceRegistration : hideServiceRegistration,
        showServiceRegistration : showServiceRegistration
    }

}();

$('#btn-cancel-service-registration').on("click", function() {
    document.getElementById('btn-add-service').style.display = "inline";
    BarberShopHandler.hideServiceRegistration();
});

$('#btn-save-service-registration').on("click", function() {
    document.getElementById('btn-add-service').style.display = "inline";
    ShirimeUtils.invalidateFieldsSemValidationDiv(BarberShopHandler.C.divServiceRegistrationId);
    if ($("#" + BarberShopHandler.C.divServiceRegistrationId).find('.invalid').length == 0) {
        BarberShopHandler.saveService();
    };
});

$('.money').mask("0.000,00", {reverse: true});

$('.time-length').mask("00:00", {reverse: true});

$("#service-registration-description").on("keyup", function() {
    var serviceText = $("#service-registration-description").val();
    if (serviceText.length < 3) {
        ShirimeUtils.invalidateField('service-registration-description');
    }
    else {
        ShirimeUtils.validateField('service-registration-description');
    }
});

$("#service-registration-price").on("keyup", function() {
    var priceText = $("#service-registration-price").val();
    var formattedPrice = priceText.replace('.', '').replace(',', '.');
    if (priceText.length < 4 || parseFloat(formattedPrice) == 0) {
        ShirimeUtils.invalidateField('service-registration-price');
    }
    else {
        ShirimeUtils.validateField('service-registration-price');
    }
});

$("#service-registration-time-length").on("keyup", function() {
    var timeLengthText = $("#service-registration-time-length").val();
    if (timeLengthText.length < 4 || timeLengthText == '00:00') {
        ShirimeUtils.invalidateField('service-registration-time-length');
    }
    else {
        ShirimeUtils.validateField('service-registration-time-length');
    }
});
