var TimeTableHandler = function() {

    var saveWorkHours = function(start, end, id) {
        var isUpdate = id != null;
        params = {
            'start' : start,
            'end' : end
        };
        if (isUpdate) {
            params['id'] = id;
        }
        $.post(
            ShirimeUtils.appBaseUrl() + "/barbershop/workhours/save",
            params,
            function(data, status, xhr) {
                if (!isUpdate) {
                    $('#calendar').fullCalendar('renderEvent', data);
                }
            }
        )
    }

    var removeWorkHours = function(workHoursId) {
        params = {'workHoursId' : workHoursId};
        $.post(
            ShirimeUtils.appBaseUrl() + "/barbershop/workhours/remove",
            params,
            function(data, status, xhr) {
                if (isUpdate) {
                    $('#calendar').fullCalendar('removeEvents', workHoursId);
                }
            }
        )
    }

    return {
        saveWorkHours : saveWorkHours,
        removeWorkHours : removeWorkHours
    }

}();

$(function() {
    $('#calendar').fullCalendar({
        firstDay: 'today',
        slotLabelFormat: 'HH:mm',
        defaultView: 'agendaWeek',
        titleFormat: 'YYYY',
        columnHeaderFormat: 'ddd D/M',
        contentHeight:"auto",
        selectable: true,
        selectOverlap: false,
        selectHelper: true,
        allDaySlot: false,
        slotEventOverlap: false,
        minTime: "06:00:00",
        locale: "pt-br",
        displayEventTime: true,
        displayEventEnd: true,
        editable: true,
        eventOverlap: false,
        eventColor: '#000000',
        eventBorderColor: '#db9356',
        header: {
            left: "none",
            right: "today prev next"
        },
        selectConstraint:{
            start: '00:01',
            end: '23:59',
        },
        viewRender: function (view) {
            var today = $.fullCalendar.moment();
            // view.end seems to be +1 day than the displayed, hence the "<="
            if (view.start <= today) {
                $('#calendar').fullCalendar('gotoDate', today);
                $('.fc-prev-button').addClass('fc-state-disabled');
            }
        },
        eventRender: function ( event, element ) {
            element.attr( 'id', event.id );
        },
        select: function(start, end, jsEvent, view) {
            TimeTableHandler.saveWorkHours(start.toISOString(), end.toISOString());
        },
        eventDragStart: function(event, jsEvent) {
            $('#recycle-bin').show();
        },
        dragRevertDuration: 0,
        eventDragStop: function(event, jsEvent, ui, view) {
            var bin = $('#recycle-bin');
            var ofs = bin.offset();
            var x1 = ofs.left;
            var x2 = ofs.left + bin.outerWidth(true);
            var y1 = ofs.top;
            var y2 = ofs.top + bin.outerHeight(true);
            if (jsEvent.pageX >= x1 && jsEvent.pageX<= x2 && jsEvent.pageY >= y1 && jsEvent.pageY <= y2) {
                $('#calendar').fullCalendar('removeEvents', event.id);
                TimeTableHandler.removeWorkHours(event.id);
            };
            $('#recycle-bin').hide();
        },
        eventDrop: function(event, delta, revertFunc) {
            TimeTableHandler.saveWorkHours(event.start.toISOString(), event.end.toISOString(), event.id);
        },
        eventResize: function(event, delta, revertFunc) {
            TimeTableHandler.saveWorkHours(event.start.toISOString(), event.end.toISOString(), event.id);
        },
        events: {
            url: ShirimeUtils.appBaseUrl() + '/barbershop/workhours/get',
            type: 'GET'
        }
    });
});
