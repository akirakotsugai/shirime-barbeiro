<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="<c:url value='/resources/css/barbershop-services.css'/>" rel="stylesheet">

<div id="service-registration" style="display: none;">
    <h3 id="service-registration-title">Novo</h3>
    <form>
        <label for="service-registration-description">Serviço</label>
        <input id="service-registration-description" class="form-text" name="description"/>
        <label for="service-registration-price">Preço R$</label>
        <input id="service-registration-price" class="form-text money" name="price" value="00,00"/>
        <label for="service-registration-time-length">Tempo HR:MIN</label>
        <input id="service-registration-time-length" class="form-text time-length" name="minutes" value="00:00"/>
        <input type="hidden" id="service-registration-id" name="id"/>
    </form>
    <div class="save-cancel">
        <button id="btn-cancel-service-registration" class="btn-shirime">cancelar</button>
        <button id="btn-save-service-registration" class="btn-shirime">salvar</button>
    </div>
</div>

<div id="services-table">
    <jsp:include page="services-table.jsp">
        <jsp:param name="services" value="${param.services}"/>
    </jsp:include>
</div>
