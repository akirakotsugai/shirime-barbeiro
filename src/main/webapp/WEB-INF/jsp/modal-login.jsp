<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="modal-login" class="modal-login">
  <div class="modal-login-window">
      <!-- Modal header -->
    <div class="modal-login-header">
      <span class="modal-close">&times;</span>
      <h3 id="modal-login-title">Opa</h3>
    </div>
      <!-- Modal content -->
    <div class="modal-login-content">
      <form id="user-form">
        <div id="choose-user-type">
        <ul>
          <li>
            <button type="button" id="i-am-a-barber">Sou barbeiro</button>
          </li>
          <li>
            <button type="button" id="i-am-a-client">Sou cliente</button>
          </li>
        </ul>
        </div>
        <div id="user-signin" class="form-column">
          <label for="login-login">Login</label>
          <input id="login-login" class="form-text" name="login" title="Somente letras são permitidos, com um mínimo de 5 dígitos" autofocus>
          <label for="login-password">Senha</label>
          <input id="login-password" class="form-text" name="password" type="password">
          <br>
          <span id="authentication-error" style="color:#C0392B; display:none;">Usuário ou senha inválidos.</span>
          <br>
          <a id="signup-link" href="#">Ainda não tenho uma conta.</a>
        </div>
        <input id="user-type-registration" type="hidden" name="type">
        <div id="personal-info" class="form-column">
          <label for="login-registration">Login</label>
          <input id="login-registration" class="form-text" name="login" title="Somente letras são permitidos, com um mínimo de 5 dígitos" autofocus>
          <label for="password-registration">Senha</label>
          <input id="password-registration" class="form-text" name="password" type="password">
          <label for="password-again-registration">Repita a password</label>
          <input id="password-again-registration" class="form-text" name="password-novamente" type="password">
          <label for="name-registration">Nome</label>
          <input id="name-registration" class="form-text" name="name">
          <label for="surname-registration">Sobrenome</label>
          <input id="surname-registration" class="form-text" name="surname">
          <label for="date-of-birth-registration">Nascimento</label>
          <input id="date-of-birth-registration" class="form-text" name="dateOfBirth">
          <label for="email-registration">Email</label>
          <input id="email-registration" class="form-text" name="email">
          <label for="cpf-registration">CPF</label>
          <input id="cpf-registration" class="form-text" name="cpf">
        </div>
        <div id="barberShop-info" class="form-column">
          <label for="barbershop-name-registration">Nome da Barbearia</label>
          <input id="barbershop-name-registration" class="form-text" name="barberShop.name" autofocus>
          <label for="cnpj-registration">CNPJ</label>
          <input id="cnpj-registration" class="form-text" name="barberShop.cnpj" autofocus>
          <label for="cep-registration">CEP</label>
          <input id="cep-registration" class="form-text" name="barberShop.address.zipCode">
          <label for="state-registration">Estado</label>
          <input id="state-registration" class="form-text address-auto-completable" name="barberShop.address.state" disabled>
          <label for="city-registration">Cidade</label>
          <input id="city-registration" class="form-text address-auto-completable" name="barberShop.address.city" disabled>
          <label for="district-registration">Bairro</label>
          <input id="district-registration" class="form-text address-auto-completable" name="barberShop.address.district" disabled>
          <label for="address-registration">Endereço</label>
          <input id="address-registration" class="form-text address-auto-completable" name="barberShop.address.street" disabled>
          <label for="number-registration">Número</label>
          <input id="number-registration" class="form-text" name="barberShop.address.number">
        </div>
        <button id="btn-login" class="btn-shirime" form="user-form" type="button">Entrar</button>
      </form>
    </div>
      <!-- Modal footer -->
    <div class="modal-login-footer">
    </div>
  </div>
</div>
