<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="pt">
    <head>
        <title>Shirime</title>
        <link href="<c:url value='/resources/img/barber-icon.png'/>" rel="icon">
        <link href="<c:url value='/resources/css/reset.css'/>" rel="stylesheet">
        <link href="<c:url value='/resources/css/index.css'/>" rel="stylesheet">
        <link href="<c:url value='/resources/css/modal-login.css'/>" rel="stylesheet">
        <link href="<c:url value='/resources/css/shirime.css'/>" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Crimson+Text:400,400italic,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rye" rel="stylesheet">
        <script type="text/javascript" src="<c:url value='/resources/js/jquery-3.3.1.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/resources/js/jquery.mask.min.js'/>"></script>
        <script src="http://maps.google.com/maps/api/js"></script>
    </head>
    <body>
        <jsp:include page="modal-login.jsp"/>
        <header>
            <h1>Shirime</h1>
            <button id="btn-signin">Entrar</button>
            <p class="subtitle">Encontre e agende serviços em barbearias.</p>
        </header>
        <main>
        </main>
    </body>
    <script src="<c:url value='/resources/js/shirime-utils.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/resources/js/login.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/resources/js/index.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/resources/js/user.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/resources/js/barbershop.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/resources/js/localizacao.js'/>" type="text/javascript"></script>
</html>
