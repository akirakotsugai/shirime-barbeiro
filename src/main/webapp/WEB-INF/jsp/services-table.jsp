<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h3>Serviços</h3>
<table>
    <thead>
        <tr>
            <th>Serviço</th>
            <td class="price-title">Preço</th>
            <td class="time-length-title">Tempo</th>
            <td>
                <button id="btn-add-service" onclick="BarberShopHandler.showServiceRegistration();">+</button>
            </td>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="service" items="${model.services}">
            <tr>
                <th>${service.description}</th>
                <td>${service.formattedPrice}</td>
                <td>${service.formattedTimeLength}</td>
                <td><button class="btn-remove-service" onclick="BarberShopHandler.removeService(${service.id})">-</button></td>
                <td><button class="btn-edit-service" onclick="BarberShopHandler.showServiceRegistration(${service.id})">✎</button></td>
            </tr>
            <div id="service_${service.id}" style="display: none;">
                <input class="serviceDescription" value="${service.description}"/>
                <input class="servicePrice" value="${service.price}"/>
                <input class="serviceMinutes" value="${service.minutes}"/>
            </div>
        </c:forEach>
    </tbody>
</table>