<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="<c:url value='/resources/css/fullcalendar.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/barbershop-timetable.css'/>" rel="stylesheet">
<script type="text/javascript" src="<c:url value='resources/js/moment.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='resources/js/fullcalendar.js'/>"></script>
<script type="text/javascript" src="<c:url value='resources/js/locale-all.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/barbershop-timetable.js'/>"></script>

<div id="recycle-bin-div">
    <img id="recycle-bin" src="<c:url value='/resources/img/recycle-bin.png'/>" alt="">
</div>
<div id="calendar"></div>