<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />

<html lang="pt">
    <head>
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/">
        <title>${model.barberShop.name}</title>
        <link href="<c:url value='/resources/img/barber-icon.png'/>" rel="icon">
        <link href="<c:url value='/resources/css/reset.css'/>" rel="stylesheet">
        <link href="<c:url value='/resources/css/barbershop-base.css'/>" rel="stylesheet">
        <link href="<c:url value='/resources/css/shirime.css'/>" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Crimson+Text:400,400italic,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rye" rel="stylesheet">
        <script type="text/javascript" src="<c:url value='/resources/js/jquery-3.3.1.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/resources/js/jquery.mask.min.js'/>"></script>
    </head>

    <body id="barbershop-body">
        <header id="barbershop-header">
          <div id="shirime-logo">
              <h1>Shirime<br>Barbeiro</h1>
          </div>
          <div id="title-barberShop">
              <h2>${model.barberShop.name}</h2>
          </div>
          <nav id="barbershop-nav">
            <ul>
              <li><a href="#"><span>Home</span></a></li>
              <li><a href="#"><span>Perfil</span></a></li>
              <li><a href="barbershop/timetable"><span>Horários</span></a></li>
              <li><a href="barbershop/services"><span>Serviços</span></a></li>
            </ul>
          </nav>
        </header>
        <main id="barbershop-main">
            <div id="barbershop-conteudo">
                <jsp:include page="${model.screen ne null ? model.screen : 'barbershop-home'}.jsp">
                    <jsp:param name="services" value="${model.services}"/>
                </jsp:include>
            </div>
        </main>
    </body>
    <input type="hidden" id="barberShopId" value="${model.barberShop.id}"/>
    <script src="<c:url value='/resources/js/barbershop.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/resources/js/shirime-utils.js'/>" type="text/javascript"></script>
</html>
