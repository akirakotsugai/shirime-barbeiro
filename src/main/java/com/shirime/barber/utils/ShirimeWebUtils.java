package com.shirime.barber.utils;

import com.shirime.barber.dto.AddressDTO;
import com.shirime.barber.pojo.Location;
import org.json.JSONObject;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URL;
import java.util.Scanner;

public class ShirimeWebUtils {

    public static void fillInCoordinates(AddressDTO addressDTO) throws Exception {
        String zipCode = addressDTO.getZipCode();
        Location location = getCoordinates(zipCode);
        addressDTO.setLatitude(location.getLatitude());
        addressDTO.setLongitude(location.getLongitude());
    }

    private static Location getCoordinates(String address) throws Exception {
        String linkApi = "https://maps.googleapis.com/maps/api/geocode/json";
        String apiKey = "AIzaSyBloHt84KfH6dgUTu2rFz0tcBIVv5ch9PQ";
        UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(linkApi);
        urlBuilder.queryParam("address", address);
        urlBuilder.queryParam("key", apiKey);
        String url = urlBuilder.build(false).encode().toUriString();
        URL requestUrl = new URL(url);
        Scanner scanner = new Scanner(requestUrl.openStream());
        String response = scanner.useDelimiter("\\Z").next();
        scanner.close();
        JSONObject result = new JSONObject(response).getJSONArray("results").getJSONObject(0);
        Double latitude = (Double) result.getJSONObject("geometry").getJSONObject("location").get("lat");
        Double longitude = (Double) result.getJSONObject("geometry").getJSONObject("location").get("lng");
        Location location = new Location();
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return location;
    }
}
