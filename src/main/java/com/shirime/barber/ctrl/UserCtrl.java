package com.shirime.barber.ctrl;

import com.shirime.barber.dto.UserDTO;
import com.shirime.barber.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.Map;

import static com.shirime.barber.ctrl.BarberShopCtrl.VIEW_BARBERSHOP_BASE;

@Controller
@RequestMapping("/user")
public class UserCtrl {

    @Autowired
    private UserService userService;

    private static Logger log = Logger.getLogger(UserCtrl.class);

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public Object authenticate(@RequestParam("login") String login, @RequestParam("password") String password, HttpSession sessao) {
        UserDTO user = userService.searchUser(login, password);
        log.info("Is user valid?= " + user != null);
        if (user != null) {
            sessao.setAttribute("user", user);
            Map<String, Object> model = new HashMap<>();
            model.put("barberShop", user.getBarberShop());
            return new ModelAndView(VIEW_BARBERSHOP_BASE, "model", model);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
    @CrossOrigin
    @ResponseBody
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity register(@ModelAttribute UserDTO userDTO) {
        boolean newBarber = userDTO.getBarberShop() != null;
        try {
            this.userService.saveOwner(userDTO, newBarber);
            log.info("user " + userDTO.getName() + " saved successfully.");
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e){
            log.info("It wasn't possible to save the user "+ userDTO.getName());
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @CrossOrigin
    @ResponseBody
    @RequestMapping(value = "/availability/login", method = RequestMethod.GET)
    public ResponseEntity<Void> checkLoginAvailability(@RequestParam("login") String login) {
        boolean loginExists = userService.checkIfLoginExists(login);
        return new ResponseEntity<>(loginExists ? HttpStatus.BAD_REQUEST : HttpStatus.OK);
    }

    @CrossOrigin
    @ResponseBody
    @RequestMapping(value = "/availability/cpf", method = RequestMethod.GET)
    public ResponseEntity<Void> checkCpfAvailability(@RequestParam("cpf") String cpf) {
        boolean cpfExists = userService.checkIfCpfExists(cpf);
        return new ResponseEntity<>(cpfExists ? HttpStatus.BAD_REQUEST : HttpStatus.OK);
    }

}