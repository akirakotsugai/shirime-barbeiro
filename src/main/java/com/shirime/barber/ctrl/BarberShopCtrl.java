package com.shirime.barber.ctrl;

import com.shirime.barber.dto.BarberShopDTO;
import com.shirime.barber.dto.ServiceDTO;
import com.shirime.barber.dto.TimePeriodDTO;
import com.shirime.barber.dto.UserDTO;
import com.shirime.barber.service.BarberShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Controller
@RequestMapping(value = "/barbershop")
public class BarberShopCtrl {

    @Autowired
    BarberShopService barberShopService;

    public static final String VIEW_BARBERSHOP_BASE = "barbershop-base";
    private static final String VIEW_BARBERSHOP_TIMETABLE = "barbershop-timetable";
    private static final String VIEW_BARBERSHOP_SERVICES = "barbershop-services";
    private static final String VIEW_SERVICES_TABLE = "services-table";

    public ModelAndView barberShop(Map<String, Object> model) {
        return new ModelAndView(VIEW_BARBERSHOP_BASE, "model", model);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView getHomePage(HttpSession session) {
        Map<String, Object> model = this.buildBaseModel(session);
        return barberShop(model);
    }

    @RequestMapping(value = "/timetable", method = RequestMethod.GET)
    public ModelAndView getTimetablePage(HttpSession session) {
        Map<String, Object> model = this.buildBaseModel(session);
        model.put("screen", VIEW_BARBERSHOP_TIMETABLE);
        return barberShop(model);
    }

    @RequestMapping(value = "/services", method = RequestMethod.GET)
    public ModelAndView getServicesPage(HttpSession session) {
        Map<String, Object> model = this.buildBaseModel(session);
        model.put("screen", VIEW_BARBERSHOP_SERVICES);
        BarberShopDTO barberShop = (BarberShopDTO) model.get("barberShop");
        this.fillModelWithServices(barberShop.getId(), model);
        return barberShop(model);
    }

    @CrossOrigin
    @RequestMapping(value = "/services/table", method = RequestMethod.POST)
    public ModelAndView getServicesTable(@RequestParam("barberShopId") Long barberShopId) {
        return buildServicesScreen(barberShopId, true);
    }

    @RequestMapping(value = "/services/save", method = RequestMethod.POST)
    public ModelAndView saveService(@ModelAttribute ServiceDTO serviceDTO) {
        Long barberShopId = serviceDTO.getBarberShop().getId();
        this.barberShopService.saveService(barberShopId, serviceDTO);
        return this.buildServicesScreen(barberShopId, true);
    }

    @RequestMapping(value="/services/remove", method = RequestMethod.POST)
    public ModelAndView removeService(
            @RequestParam("serviceId") Long serviceId, @RequestParam("barberShopId") Long barberShopId) {
        this.barberShopService.removeService(serviceId);
        return this.buildServicesScreen(barberShopId, true);
    }

    @CrossOrigin
    @RequestMapping(value = "/availability/name", method = RequestMethod.GET)
    public ResponseEntity<Void> checkNameAvailability(@RequestParam("name") String name) {
        boolean nameExists = barberShopService.checkIfNameExists(name);
        return new ResponseEntity<>(nameExists ? HttpStatus.BAD_REQUEST : HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/availability/cnpj", method = RequestMethod.GET)
    public ResponseEntity<Void> checkCnpjAvailability(@RequestParam("cnpj") String cnpj) {
        boolean cnpjExists = barberShopService.checkIfCnpjExists(cnpj);
        return new ResponseEntity<>(cnpjExists ? HttpStatus.BAD_REQUEST : HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/workhours/save", method = RequestMethod.POST)
    @ResponseBody
    public TimePeriodDTO saveWorkHours(@ModelAttribute TimePeriodDTO timePeriod, HttpSession session) {
        UserDTO user = (UserDTO) session.getAttribute("user");
        Long barberShopId = user.getBarberShop().getId();
        return this.barberShopService.saveWorkHours(barberShopId, timePeriod);
    }

    @CrossOrigin
    @RequestMapping(value = "/workhours/get", method = RequestMethod.GET)
    @ResponseBody
    public List<TimePeriodDTO> getWorkHours(String start, String end, HttpSession session) {
        UserDTO user = (UserDTO) session.getAttribute("user");
        Long barberShopId = user.getBarberShop().getId();
        return this.barberShopService.getWorkHours(barberShopId, start, end);
    }

    @CrossOrigin
    @RequestMapping(value = "/workhours/remove", method = RequestMethod.POST)
    public ResponseEntity<Void> removeWorkHours(Long workHoursId, HttpSession session) {
        boolean removed = this.barberShopService.removeWorkHours(workHoursId) != null;
        return new ResponseEntity<>(removed ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    // auxiliary methods

    private Map<String, Object> buildBaseModel(HttpSession session) {
        Map<String, Object> model = new HashMap<String, Object>();
        UserDTO user = (UserDTO) session.getAttribute("user");
        BarberShopDTO barberShop = user.getBarberShop();
        model.put("barberShop", user.getBarberShop());
        return model;
    }

    private void fillModelWithServices(Long barberShopId, Map<String, Object> model) {
        List<ServiceDTO> services = this.barberShopService.getServices(barberShopId);
        model.put("services", services);
    }

    private ModelAndView buildServicesScreen(Long barberShopId, boolean onlyTable) {
        Map<String, Object> model = new HashMap<>();
        this.fillModelWithServices(barberShopId, model);
        return new ModelAndView(onlyTable ? VIEW_SERVICES_TABLE : VIEW_BARBERSHOP_SERVICES, "model", model);
    }

}
