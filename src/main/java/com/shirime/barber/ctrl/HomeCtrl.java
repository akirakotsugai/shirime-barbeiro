package com.shirime.barber.ctrl;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeCtrl {

    private static final String VIEW_PAGINA_PRINCIPAL = "index";

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView obterPaginaInicial() {
        return new ModelAndView(VIEW_PAGINA_PRINCIPAL, "output", null);
    }
}
