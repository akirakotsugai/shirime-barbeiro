package com.shirime.barber.pojo;

import javax.persistence.*;
import java.io.Serializable;

import static com.shirime.barber.pojo.Service.SERVICES_BY_BARBERSHOP;

@Entity
@Table(name = "service")
@NamedQueries({
        @NamedQuery(name = SERVICES_BY_BARBERSHOP, query = "FROM Service s WHERE s.barberShop.id = :barberShopId")
})
public class Service implements Serializable {

    public static final String SERVICES_BY_BARBERSHOP = "get_services_by_barbershop";

    private long id;
    private String description;
    private Double price;
    private int minutes;
    private BarberShop barberShop;

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="service_sequence")
    @SequenceGenerator(name="service_sequence", sequenceName="service_seq")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    @ManyToOne
    @JoinColumn(name = "barberShop_id")
    public BarberShop getBarberShop() {
        return barberShop;
    }

    public void setBarberShop(BarberShop barberShop) {
        this.barberShop = barberShop;
    }
}
