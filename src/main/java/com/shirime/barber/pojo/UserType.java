package com.shirime.barber.pojo;

public enum UserType {
    ADMIN(1),
    BARBER(2),
    CLIENT(3);

    private final int code;

    UserType(int i) {
        this.code = i;
    }

    public int getCode() { return this.code; }
}
