package com.shirime.barber.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static com.shirime.barber.pojo.User.*;

@Entity
@Table(name = "user")
@NamedQueries({
        @NamedQuery(name = PROPERTIES_SEARCH, query = "FROM User u WHERE (:userLogin is null or u.login = :userLogin) and (:userPassword is null or u.password = :userPassword) and (:userType is null or u.type = :userType) and (:userName is null or u.name = :userName) and (:userSurname is null or u.surname = :userSurname) and (:userCPF is null or u.cpf = :userCPF) and (:userEmail is null or u.email = :userEmail) and (:userDateOfBirth is null or u.dateOfBirth = :userDateOfBirth)")
})
public class User implements Serializable {

    public static final String PROPERTIES_SEARCH = "search_users_by_their_properties";

    private long id;
    private String login;
    private String password;
    private int type;
    private String name;
    private String surname;
    private String cpf;
    private String email;
    private Date dateOfBirth;
    private BarberShop barberShop;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_sequence")
    @SequenceGenerator(name="user_sequence", sequenceName="user_seq")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Temporal(TemporalType.DATE)
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @ManyToOne
    @JoinColumn(name = "barberShop_id")
    public BarberShop getBarberShop() {
        return barberShop;
    }

    public void setBarberShop(BarberShop barberShop) {
        this.barberShop = barberShop;
    }
}
