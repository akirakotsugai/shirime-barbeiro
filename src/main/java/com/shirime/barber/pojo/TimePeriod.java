package com.shirime.barber.pojo;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

import static com.shirime.barber.pojo.TimePeriod.TIME_PERIODS_SEARCH;

@Entity
@Table(name = "time_period")
@NamedQueries({
        @NamedQuery(name = TIME_PERIODS_SEARCH, query = "FROM TimePeriod tp WHERE tp.barberShop.id = :barberShopId and tp.start >= :start and tp.end <= :end")
})
public class TimePeriod {

    Long id;
    LocalDateTime start;
    LocalDateTime end;
    BarberShop barberShop;

    public static final String TIME_PERIODS_SEARCH = "time_periods_given_start_and_end";

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="time_period_sequence")
    @SequenceGenerator(name="time_period_sequence", sequenceName="time_period_seq")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    @ManyToOne
    @JoinColumn(name = "barberShop_id")
    public BarberShop getBarberShop() {
        return barberShop;
    }

    public void setBarberShop(BarberShop barberShop) {
        this.barberShop = barberShop;
    }

}