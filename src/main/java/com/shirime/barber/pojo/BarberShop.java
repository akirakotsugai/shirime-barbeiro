package com.shirime.barber.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static com.shirime.barber.pojo.BarberShop.PROPERTIES_SEARCH;

@Entity
@Table(name = "barberShop")
@NamedQueries({
        @NamedQuery(name = PROPERTIES_SEARCH, query = "FROM BarberShop b WHERE (:barberShopName is null or b.name = :barberShopName) and (:barberShopCNPJ is null or b.cnpj = :barberShopCNPJ)")
})
public class BarberShop implements Serializable {

    public static final String PROPERTIES_SEARCH = "search_barberShop_by_defined_properties";

    private long id;
    private List<User> users;
    private String name;
    private String cnpj;
    private Address address;
    private List<Service> services;
    private List<TimePeriod> workHours;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="barberShop_sequence")
    @SequenceGenerator(name="barberShop_sequence", sequenceName="barberShop_seq")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "barberShop")
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "barberShop")
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "barberShop")
    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "barberShop")
    public List<TimePeriod> getWorkHours() {
        return workHours;
    }

    public void setWorkHours(List<TimePeriod> workHours) {
        this.workHours = workHours;
    }
}
