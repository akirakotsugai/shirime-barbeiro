package com.shirime.barber.service;

import com.shirime.barber.dto.ServiceDTO;
import com.shirime.barber.dto.TimePeriodDTO;
import com.shirime.barber.dto.UserDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BarberShopService {

    @Transactional
    Long saveBarberShop(UserDTO barber) throws Exception;

    boolean checkIfNameExists(String name);

    boolean checkIfCnpjExists(String cnpj);

    @Transactional
    void saveService(Long barberShopId, ServiceDTO serviceDTO);

    List<ServiceDTO> getServices(Long barberShopId);

    @Transactional
    Long removeService(Long serviceId);

    @Transactional
    TimePeriodDTO saveWorkHours(Long barberShopId, TimePeriodDTO timePeriod);

    List<TimePeriodDTO> getWorkHours(Long barberShopId, String start, String end);

    @Transactional
    Long removeWorkHours(Long timePeriodId);
}