package com.shirime.barber.service;

import com.shirime.barber.converter.UserConverter;
import com.shirime.barber.dao.UserDAO;
import com.shirime.barber.dto.UserDTO;
import com.shirime.barber.pojo.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static Logger log = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDAO userDAO;
    @Autowired
    private BarberShopService barberShopService;

    @Override
    public UserDTO searchUser(String login, String password) {
        log.info("Procurando usuário no banco de info");
        UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPassword(password);
        List<User> usersFound = userDAO.searchByProperties(user);
        if(usersFound.size() != 0) {
            User userFound = usersFound.get(0);
            log.info("User " + userFound.getLogin() + " found.");
            return UserConverter.convertEntityToDTO(userFound);
        }
        log.error("User not found in the database");
        return null;
    }

    @Override
    public boolean checkIfLoginExists(String login) {
        UserDTO userDTO = new UserDTO();
        userDTO.setLogin(login);
        return userDAO.searchByProperties(userDTO).size() != 0;
    }

    @Override
    public boolean checkIfCpfExists(String cpf) {
        UserDTO userDTO = new UserDTO();
        userDTO.setCpf(cpf);
        return userDAO.searchByProperties(userDTO).size() != 0;
    }

    @Override
    @Transactional
    public void saveUser(UserDTO userDTO) {
        User user = UserConverter.convertDTOToEntity(userDTO);
        this.userDAO.persist(user);
    }

    @Override
    @Transactional
    public void saveOwner(UserDTO userDTO, boolean newBarberShop) throws Exception {
        //if barberShop is being registered along with its first barber
        if (newBarberShop) {
            this.barberShopService.saveBarberShop(userDTO);
        }
    }
}
