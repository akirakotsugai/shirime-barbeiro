package com.shirime.barber.service;

import com.shirime.barber.converter.*;
import com.shirime.barber.dao.BarberShopDAO;
import com.shirime.barber.dao.ServiceDAO;
import com.shirime.barber.dao.TimePeriodDAO;
import com.shirime.barber.dto.*;
import com.shirime.barber.pojo.*;
import com.shirime.barber.utils.ShirimeWebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Service
public class BarberShopServiceImpl implements BarberShopService {

    @Autowired
    private BarberShopDAO barberShopDAO;
    @Autowired
    private ServiceDAO serviceDAO;
    @Autowired
    private TimePeriodDAO timePeriodDAO;

    @Override
    @Transactional
    public Long saveBarberShop(UserDTO barberDTO) throws Exception {
        User user = UserConverter.convertDTOToEntity(barberDTO);
        BarberShop barberShop = BarberShopConverter.convertDTOToEntity(barberDTO.getBarberShop());
        AddressDTO addressDTO = barberDTO.getBarberShop().getAddress();
        if (addressDTO.getLatitude() == null || addressDTO.getLongitude() == null) {
            ShirimeWebUtils.fillInCoordinates(addressDTO);
        }
        Address address = AddressConverter.convertDTOToEntity(addressDTO);
        user.setBarberShop(barberShop);
        List<User> users = new ArrayList();
        users.add(user);
        barberShop.setUsers(users);
        address.setBarberShop(barberShop);
        barberShop.setAddress(address);
        this.barberShopDAO.persist(barberShop);
        return barberShop.getId();
    }

    @Override
    public boolean checkIfNameExists(String name) {
        BarberShopDTO barberShopDTO = new BarberShopDTO();
        barberShopDTO.setName(name);
        return barberShopDAO.searchForProperties(barberShopDTO).size() != 0;
    }

    @Override
    public boolean checkIfCnpjExists(String cnpj) {
        BarberShopDTO barberShopDTO = new BarberShopDTO();
        barberShopDTO.setCnpj(cnpj);
        return barberShopDAO.searchForProperties(barberShopDTO).size() != 0;
    }

    @Override
    @Transactional
    public void saveService(Long barberShopId, ServiceDTO serviceDTO) {
        boolean isEditing = serviceDTO.getId() != null;
        BarberShop barberShop = (BarberShop) this.barberShopDAO.findById(barberShopId);
        List<Service> barberShopServices = barberShop.getServices();
        Service service = null;
        if (!isEditing) {
            service = new Service();
            barberShopServices.add(service);
        }
        else {
            for (Service sv : barberShopServices) {
                if (serviceDTO.getId().equals(sv.getId())) {
                    service = sv;
                    break;
                }
            }
        }
        service.setDescription(serviceDTO.getDescription());
        service.setPrice(serviceDTO.getPrice());
        service.setMinutes(serviceDTO.getMinutes());
        service.setBarberShop(barberShop);
        if (!isEditing) {
            this.barberShopDAO.persist(barberShop);
        }
    }

    @Override
    public List<ServiceDTO> getServices(Long barberShopId) {
        List<Service> services = this.serviceDAO.getByBarberShop(barberShopId);
        List<ServiceDTO> servicesDTO = new ArrayList<>();
        for (Service service : services) {
            servicesDTO.add(ServiceConverter.convertEntityToDTO(service));
        }
        return servicesDTO;
    }

    @Override
    @Transactional
    public Long removeService(Long serviceId) {
        try {
            Service service = (Service) this.serviceDAO.findById(serviceId);
            this.serviceDAO.remove(service);
            return serviceId;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @Transactional
    public TimePeriodDTO saveWorkHours(Long barberShopId, TimePeriodDTO workHoursDTO) {
        boolean isEditing = workHoursDTO.getId() != null;
        TimePeriod workHours;
        if (isEditing) {
            workHours = (TimePeriod) this.timePeriodDAO.findById(workHoursDTO.getId());
        }
        else {
            workHours = new TimePeriod();
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        LocalDateTime start = LocalDateTime.parse(workHoursDTO.getStart(), formatter);
        workHours.setStart(start);
        LocalDateTime end = LocalDateTime.parse(workHoursDTO.getEnd(), formatter);
        workHours.setEnd(end);
        if (!isEditing) {
            BarberShop barberShop = (BarberShop) this.barberShopDAO.findById(barberShopId);
            workHours.setBarberShop(barberShop);
            this.timePeriodDAO.persist(workHours);
            this.timePeriodDAO.getEm().flush();
            workHoursDTO.setId(workHours.getId());
        }
        return workHoursDTO;
    }

    @Override
    public List<TimePeriodDTO> getWorkHours(Long barberShopId, String start, String end) {
        List <TimePeriod> timePeriods = this.timePeriodDAO.findWorkHours(barberShopId, start, end);
        List<TimePeriodDTO> timePeriodDTOs = new ArrayList();
        for (TimePeriod timePeriod : timePeriods) {
            timePeriodDTOs.add(TimePeriodConverter.convertEntityToDTO(timePeriod));
        }
        return timePeriodDTOs;
    }

    @Override
    @Transactional
    public Long removeWorkHours(Long timePeriodId) {
        try {
            TimePeriod timePeriod = (TimePeriod) this.timePeriodDAO.findById(timePeriodId);
            this.timePeriodDAO.remove(timePeriod);
            return timePeriodId;
        } catch (Exception e) {
            return null;
        }
    }
}