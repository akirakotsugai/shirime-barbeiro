package com.shirime.barber.service;

import com.shirime.barber.dto.UserDTO;

import javax.transaction.Transactional;
import java.io.IOException;

public interface UserService {
    UserDTO searchUser(String login, String password);

    boolean checkIfLoginExists(String login);

    boolean checkIfCpfExists(String cpf);

    @Transactional
    void saveUser(UserDTO userDTO);

    @Transactional
    void saveOwner(UserDTO userDTO, boolean newBarberShop) throws IOException, Exception;
}
