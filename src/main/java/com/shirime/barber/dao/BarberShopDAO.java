package com.shirime.barber.dao;

import com.shirime.barber.dto.BarberShopDTO;
import com.shirime.barber.pojo.BarberShop;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class BarberShopDAO extends GenericDAO {

    public BarberShopDAO() {
        super(BarberShop.class);
    }

    public List<BarberShop> searchForProperties(BarberShopDTO barberShop) {
        Query query = super.getEm().createNamedQuery(BarberShop.PROPERTIES_SEARCH);
        query.setParameter("barberShopName", barberShop.getName());
        query.setParameter("barberShopCNPJ", barberShop.getCnpj());
        return query.getResultList();
    }

}