package com.shirime.barber.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
abstract class GenericDAO <T> {

    @PersistenceContext
    EntityManager em;

    private Class<T> persistedClass;

    public GenericDAO(Class<T> persistedClass) {
        this.persistedClass = persistedClass;
    }

    @Transactional
    public <T> void persist(T entity) {
        getEm().persist(entity);
    }

    @Transactional
    public <T> void update(T entity) {
        getEm().merge(entity);
    }

    @Transactional
    public <T> void remove(T entity) {
        getEm().remove(entity);
    }

    public <T> T findById(Long id) {
        return (T) getEm().find(this.persistedClass, id);
    }

    public <T> List<T> findAll() {
        Query query = getEm().createQuery("FROM " + this.persistedClass.getName());
        return query.getResultList();
    }

    public <T> List<T> findByNamedQuery(String namedQuery) {
        Query query = getEm().createNamedQuery(namedQuery);
        return query.getResultList();
    }

    public EntityManager getEm() {
        return this.em;
    }
}
