package com.shirime.barber.dao;

import com.shirime.barber.pojo.TimePeriod;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Repository
public class TimePeriodDAO extends GenericDAO {

    public TimePeriodDAO() {
        super(TimePeriod.class);
    }

    public TimePeriod getById(Long timePeriodId) {
        return (TimePeriod) super.findById(timePeriodId);
    }

    public List<TimePeriod> findWorkHours(Long barberShopId, String start, String end) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        LocalDateTime dtStart = LocalDateTime.parse(start, formatter);
        LocalDateTime dtEnd = LocalDateTime.parse(end, formatter);
        Query query = super.getEm().createNamedQuery(TimePeriod.TIME_PERIODS_SEARCH);
        query.setParameter("barberShopId", barberShopId);
        query.setParameter("start", dtStart);
        query.setParameter("end", dtEnd);
        return (List<TimePeriod>) query.getResultList();
    }

}
