package com.shirime.barber.dao;

import com.shirime.barber.pojo.Service;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class ServiceDAO extends GenericDAO {

    public ServiceDAO() {
        super(Service.class);
    }

    public Service getById(Long serviceId) {
        return (Service) super.findById(serviceId);
    }

    public List<Service> getByBarberShop(Long barberShopId) {
        Query query = super.getEm().createNamedQuery(Service.SERVICES_BY_BARBERSHOP);
        query.setParameter("barberShopId", barberShopId);
        return query.getResultList();
    }

}
