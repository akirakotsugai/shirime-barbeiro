package com.shirime.barber.dao;

import com.shirime.barber.dto.UserDTO;
import com.shirime.barber.pojo.User;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class UserDAO extends GenericDAO  {

    public UserDAO() {
        super(User.class);
    }

    public List<User> searchByProperties(UserDTO user) {
        //get all if empty.
        Query query = super.getEm().createNamedQuery(User.PROPERTIES_SEARCH);
        query.setParameter("userLogin", user.getLogin());
        query.setParameter("userPassword", user.getPassword());
        query.setParameter("userType", user.getType());
        query.setParameter("userName", user.getName());
        query.setParameter("userSurname", user.getSurname());
        query.setParameter("userCPF", user.getCpf());
        query.setParameter("userEmail", user.getEmail());
        query.setParameter("userDateOfBirth", user.getDateOfBirth());
        return (List<User>) query.getResultList();
    }

}
