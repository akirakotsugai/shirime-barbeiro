package com.shirime.barber.interceptor;

import com.shirime.barber.dto.UserDTO;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.shirime.barber.pojo.UserType.*;

public class ShirimeInterceptor extends HandlerInterceptorAdapter {

    @Override

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object controller) throws Exception {

        String uri = request.getRequestURI();
        UserDTO user = (UserDTO) request.getSession().getAttribute("user");
        if (uri.contains("/ShirimeBarber/resources")) {
            return true;
        }

        if (uri.contains("barbershop")) {
            if (user != null && user.getType().equals(BARBER.getCode())) {
                return true;
            }
            if (isAjax(request)) {
                response.setHeader("AUTH", "false");
            } else {
                response.sendRedirect("/ShirimeBarber/");
            }
            return false;
        }

        if (uri.equals("/ShirimeBarber/") && user != null && user.getType().equals(BARBER.getCode())) {
            response.sendRedirect("/ShirimeBarber/barbershop/");
        }
        return true;
    }

    private boolean isAjax(HttpServletRequest request) {
        String requestedWithHeader = request.getHeader("X-Requested-With");
        return "XMLHttpRequest".equals(requestedWithHeader);
    }

}
