package com.shirime.barber.converter;

import com.shirime.barber.dto.AddressDTO;
import com.shirime.barber.pojo.Address;
import org.springframework.beans.BeanUtils;

public class AddressConverter {

    public static AddressDTO convertEntityToDTO(Address address) {
        AddressDTO addressDTO = new AddressDTO();
        BeanUtils.copyProperties(address, addressDTO);
        return addressDTO;
    }

    public static Address convertDTOToEntity(AddressDTO addressDTO) {
        Address address = new Address();
        address.setZipCode(addressDTO.getZipCode());
        address.setState(addressDTO.getState());
        address.setCity(addressDTO.getCity());
        address.setStreet(addressDTO.getStreet());
        address.setNumber(addressDTO.getNumber());
        address.setLatitude(addressDTO.getLatitude());
        address.setLongitude(addressDTO.getLongitude());
        return address;
    }
}
