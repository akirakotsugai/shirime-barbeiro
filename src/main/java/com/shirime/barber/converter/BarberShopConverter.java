package com.shirime.barber.converter;

import com.shirime.barber.dto.BarberShopDTO;
import com.shirime.barber.pojo.BarberShop;
import org.springframework.beans.BeanUtils;

public class BarberShopConverter {

    public static BarberShopDTO convertEntityToDTO(BarberShop barberShop) {
        BarberShopDTO barberShopDTO = new BarberShopDTO();
        BeanUtils.copyProperties(barberShop, barberShopDTO);
        return barberShopDTO;
    }

    public static BarberShop convertDTOToEntity(BarberShopDTO barberShopDTO) {
        BarberShop barberShop = new BarberShop();
        barberShop.setName(barberShopDTO.getName());
        barberShop.setCnpj(barberShopDTO.getCnpj());
        return barberShop;

    }
}
