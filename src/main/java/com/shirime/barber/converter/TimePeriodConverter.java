package com.shirime.barber.converter;

import com.shirime.barber.dto.TimePeriodDTO;
import com.shirime.barber.pojo.TimePeriod;

public class TimePeriodConverter {

    public static TimePeriodDTO convertEntityToDTO(TimePeriod timePeriod) {
        TimePeriodDTO timePeriodDTO = new TimePeriodDTO();
        timePeriodDTO.setId(timePeriod.getId());
        timePeriodDTO.setStart(timePeriod.getStart().toString());
        timePeriodDTO.setEnd(timePeriod.getEnd().toString());
        return timePeriodDTO;
    }

}
