package com.shirime.barber.converter;

import com.shirime.barber.dto.BarberShopDTO;
import com.shirime.barber.dto.UserDTO;
import com.shirime.barber.pojo.User;
import org.springframework.beans.BeanUtils;

public class UserConverter {

    public static UserDTO convertEntityToDTO(User user) {
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user, userDTO);
        BarberShopDTO barberShopDTO = new BarberShopDTO();
        BeanUtils.copyProperties(user.getBarberShop(), barberShopDTO);
        userDTO.setBarberShop(barberShopDTO);
        return userDTO;
    }

    public static User convertDTOToEntity(UserDTO userDTO) {
        User user = new User();
        user.setName(userDTO.getName());
        user.setSurname(userDTO.getSurname());
        user.setLogin(userDTO.getLogin());
        user.setType(userDTO.getType());
        user.setPassword(userDTO.getPassword());
        user.setCpf(userDTO.getCpf());
        user.setEmail(userDTO.getEmail());
        user.setDateOfBirth(userDTO.getDateOfBirth());
        return user;
    }
}
