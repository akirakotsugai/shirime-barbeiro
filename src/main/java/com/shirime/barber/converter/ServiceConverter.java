package com.shirime.barber.converter;

import com.shirime.barber.dto.ServiceDTO;
import com.shirime.barber.pojo.Service;
import org.springframework.beans.BeanUtils;

public class ServiceConverter {
    public static ServiceDTO convertEntityToDTO(Service service) {
        ServiceDTO serviceDTO = new ServiceDTO();
        BeanUtils.copyProperties(service, serviceDTO);
        return serviceDTO;
    }

    public static Service convertDTOToEntity(ServiceDTO serviceDTO) {
        Service service = new Service();
        BeanUtils.copyProperties(serviceDTO, service);
        return service;
    }
}
