package com.shirime.barber.dto;

import java.util.Date;

public class UserDTO {

    private Long id;
    private String login;
    private String password;
    private Integer type;
    private String name;
    private String surname;
    private String cpf;
    private String email;
    private Date dateOfBirth;
    private BarberShopDTO barberShop;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public BarberShopDTO getBarberShop() {
        return barberShop;
    }

    public void setBarberShop(BarberShopDTO barberShop) {
        this.barberShop = barberShop;
    }
}
