package com.shirime.barber.dto;

import java.util.List;

public class BarberShopDTO {

    private Long id;
    private List<UserDTO> barbers;
    private String name;
    private String cnpj;
    private AddressDTO address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<UserDTO> getBarbers() {
        return barbers;
    }

    public void setBarbers(List<UserDTO> barbers) {
        this.barbers = this.barbers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }
}
