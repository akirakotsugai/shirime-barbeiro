package com.shirime.barber.dto;

import java.text.NumberFormat;
import java.util.Locale;

public class ServiceDTO {

    private Long id;
    private String description;
    private Double price;
    private Integer minutes;
    private BarberShopDTO barberShop;

    public ServiceDTO() {}
    public ServiceDTO(String description, Double price, Integer minutes) {
        this.description = description;
        this.price = price;
        this.minutes = minutes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public BarberShopDTO getBarberShop() {
        return barberShop;
    }

    public void setBarberShop(BarberShopDTO barberShop) {
        this.barberShop = barberShop;
    }

    public String getFormattedPrice() {
        NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        return formatter.format(this.price);
    }

    public String getFormattedTimeLength() {
        int hours = this.minutes /60;
        int minutes = (this.minutes - (hours * 60));
        String formattedTimeLength = new String();
        if (hours > 0) {
            formattedTimeLength += hours + "hr ";
        }
        if (minutes > 0) {
            formattedTimeLength += minutes + "min";
        }
        return formattedTimeLength;
    }

}
